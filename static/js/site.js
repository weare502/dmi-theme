/* jshint devel: true */
/* global Modernizr */

/* jshint ignore:start */

/**
 * String.startsWith shim
 */
if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position){
      position = position || 0;
      return this.substr(position, searchString.length) === searchString;
  };
}

/**
 * FitVids JS
 */
;(function( $ ){

  'use strict';

  $.fn.fitVids = function( options ) {
    var settings = {
      customSelector: null,
      ignore: null
    };

    if(!document.getElementById('fit-vids-style')) {
      // appendStyles: https://github.com/toddmotto/fluidvids/blob/master/dist/fluidvids.js
      var head = document.head || document.getElementsByTagName('head')[0];
      var css = '.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}';
      var div = document.createElement("div");
      div.innerHTML = '<p>x</p><style id="fit-vids-style">' + css + '</style>';
      head.appendChild(div.childNodes[1]);
    }

    if ( options ) {
      $.extend( settings, options );
    }

    return this.each(function(){
      var selectors = [
        'iframe[src*="player.vimeo.com"]',
        'iframe[src*="youtube.com"]',
        'iframe[src*="youtube-nocookie.com"]',
        'iframe[src*="kickstarter.com"][src*="video.html"]',
        'object',
        'embed'
      ];

      if (settings.customSelector) {
        selectors.push(settings.customSelector);
      }

      var ignoreList = '.fitvidsignore';

      if(settings.ignore) {
        ignoreList = ignoreList + ', ' + settings.ignore;
      }

      var $allVideos = $(this).find(selectors.join(','));
      $allVideos = $allVideos.not('object object'); // SwfObj conflict patch
      $allVideos = $allVideos.not(ignoreList); // Disable FitVids on this video.

      $allVideos.each(function(){
        var $this = $(this);
        if($this.parents(ignoreList).length > 0) {
          return; // Disable FitVids on this video.
        }
        if (this.tagName.toLowerCase() === 'embed' && $this.parent('object').length || $this.parent('.fluid-width-video-wrapper').length) { return; }
        if ((!$this.css('height') && !$this.css('width')) && (isNaN($this.attr('height')) || isNaN($this.attr('width'))))
        {
          $this.attr('height', 9);
          $this.attr('width', 16);
        }
        var height = ( this.tagName.toLowerCase() === 'object' || ($this.attr('height') && !isNaN(parseInt($this.attr('height'), 10))) ) ? parseInt($this.attr('height'), 10) : $this.height(),
            width = !isNaN(parseInt($this.attr('width'), 10)) ? parseInt($this.attr('width'), 10) : $this.width(),
            aspectRatio = height / width;
        if(!$this.attr('name')){
          var videoName = 'fitvid' + $.fn.fitVids._count;
          $this.attr('name', videoName);
          $.fn.fitVids._count++;
        }
        $this.wrap('<div class="fluid-width-video-wrapper"></div>').parent('.fluid-width-video-wrapper').css('padding-top', (aspectRatio * 100)+'%');
        $this.removeAttr('height').removeAttr('width');
      });
    });
  };
  
  // Internal counter for unique video names.
  $.fn.fitVids._count = 0;
  
// Works with either jQuery or Zepto
})( window.jQuery || window.Zepto );
/* jshint ignore:end */

jQuery( document ).ready( function( $ ) {

	var $menuToggle = $( '#menu-toggle' );
	var $body = $('body');
	var $header = $('#header');

	$body.fitVids();

	$body.on('click', '#menu-toggle', function(){
		$header.toggleClass('nav-open');
		$menuToggle.toggleClass('fa-bars');
		$menuToggle.toggleClass('fa-close');
	} );

  $('#content').find('a').has('img').addClass('imgwrap');

  $('#nav-main .sub-menu').wrap('<div class="sub-menu-wrap"></div>');

  if ( Modernizr.touch ){
    $('#nav-main .container > ul li.menu-item-has-children a').after('<button class="toggle-sub-menu"><span class="screen-reader-text">Toggle Sub-Menu</span></button>');
  }
  
  // $('body').on('click', '.toggle-sub-menu', function(){
  //   // var $this = $(this);
  //   // var $parent = $this.parent('li');

  //   // if ( $(this).parent('li').hasClass('open') ){
  //   //   $parent.removeClass('open');
  //   //   return;
  //   // }

  //   // $(this).parent('li').siblings('li').removeClass('open');
  //   // $(this).parent('li').addClass('open');

  // });

  $('#sticky-maps-toggle').click(function(){
    $('.sticky-maps').toggleClass('open');
  });

  $('.toggle-card').on('click', function(){
    var action = $(this).text().slice(-4).toLowerCase();
    if ( action === "more" ){
      $(this).text('Show Less');
      $(this).closest('.card').addClass('active');
    } else {
      $(this).text('Show More');
      $(this).closest('.card').removeClass('active');
    }
  });

  if ( $('.home-featured-events .card').length > 3 ){
    $('.home-featured-events .cards').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 900,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 500,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    });
  }

  $('.home-hero-slider .image-slider').slick({
    arrows: false,
    autoplay: true,
    autoplaySpeed: 8000,
    draggable: false,
    fade: true,
    pauseOnFocus: false,
    pauseOnHover: false,
    speed: 500
  });

  $('.home-hero-slider .banner-slider').slick({
    arrows: false,
    autoplay: true,
    autoplaySpeed: 8000,
    draggable: false,
    fade: true,
    pauseOnFocus: false,
    pauseOnHover: false,
    speed: 500
  });

              

	$('.open-popup-link').magnificPopup({
		type:'inline',
		removalDelay: 300,
		midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
	});

	function showTab(e){
		e.preventDefault();
		$('.tab-content').hide();
		$('.tab-switch').removeClass('active-tab');
		var $this = $(this);
		$this.addClass('active-tab');
		var target = $this.attr('data-tab-target');
		$('[data-tab=' + target + ']').show();
	}

	$('.tab-switch').on('click', showTab);

  $('.tab-content').hide();
  $('.tab-content').first().show();
  $('.tabs .tab-switch').removeClass('active-tab');
  $('.tabs .tab-switch').first().addClass('active-tab');

  // $('.search-form').on('submit', function(e){
  //   e.preventDefault();
  //   window.open( "https://google.com/?q=site:downtownmanhattanks.com+" + $('.search-form').find('.search-query').val(), "_blank" );
  // });

}); // End Document Ready