<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$context = Timber::get_context();
$context['posts'] = Timber::get_posts();
$context['current_term'] = new TimberTerm();

$GLOBALS['dmi_context'] = $context;

if ( $context['current_term']->parent ){
	Timber::render( 'business-category.twig', $context );
} else {
	$context['terms'] = array_filter( Timber::get_terms('business_category', $context['current_term']->ID ), function( $term ){
		global $dmi_context;
		if ( 0 === $term->count ) return false;
		return ( $term->parent === $dmi_context['current_term']->ID );
	} );
	Timber::render( 'business-category-list.twig', $context );
}
