<?php

class DMI_Event extends TimberPost {

	public function is_all_day_event() {
		return tribe_event_is_all_day( $this->ID );
	}

	public function event_dates( $format = 'n/j/Y', $sep = '-' ) {

		$start = date( $format, strtotime( $this->_EventStartDate ) );
		$end = date( $format, strtotime( $this->_EventEndDate ) );

		if ( $start === $end ){
			return $start;
		} else {
			return $start . " $sep " . $end;
		}

	}

	public function event_times( $sep = "-" ){
		
		$start = tribe_get_start_time( $this->ID );
		$end =  tribe_get_end_time( $this->ID );

		if ( $start === $end || $end == false ){
			return $start;
		} else {
			return $start . " $sep " . $end;
		}

	}

	public function event_location(){
		return tribe_get_venue( $this->ID );
	}
}