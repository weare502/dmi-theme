<?php 
/**
 * Template Name: Box Landing Page
 */


$context = Timber::get_context();
$post = new TimberPost();
$post->thumbnail = $post->get_thumbnail();
$context['post'] = $post;
Timber::render( 'box-landing-page.twig', $context );