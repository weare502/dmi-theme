<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

require 'lib/DMI_Event.php';

Timber::$dirname = array('templates', 'views');

class DMISite extends TimberSite {

	function __construct() {
		// add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'title-tag' );

		add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_head', array( $this, 'admin_head_css' ) );
		add_action( 'pre_get_posts', array( $this, 'pre_get_posts' ) );
		
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'mce_buttons_2', array( $this, 'tiny_mce_buttons' ) );
		add_filter( 'tiny_mce_before_init', array( $this, 'tiny_mce_insert_formats' ) );

		add_action( 'init', function(){
			add_editor_style( get_stylesheet_uri() );
		} );

		parent::__construct();
	}

	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['logo'] = trailingslashit( get_template_directory_uri() ) . 'static/images/logo.png';
		$context['year'] = date('Y');
		$context['newsletter_form'] = function_exists('gravity_form') ? gravity_form( 1, false, false, false, false, true, 0, false ) : '';
		$context['menu'] = new TimberMenu('primary');
		$context['csscache'] = filemtime(get_stylesheet_directory() . '/style.css');
		$context['today_ymd'] = date('Ymd');
		$context['home_id'] = get_option( 'page_on_front' );
		return $context;
	}

	function after_setup_theme(){
		register_nav_menu( 'primary', 'Main Navigation' );
		register_nav_menu( 'helpful_links', 'Footer Helpful Links' );
	}

	function enqueue_scripts(){

		wp_enqueue_style( 'magnific-popup-style', get_template_directory_uri() . '/bower_components/magnific-popup/dist/magnific-popup.css', array(), '20120206' );

		wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/bower_components/magnific-popup/dist/jquery.magnific-popup.min.js', array( 'jquery' ), '20120206', true );
		
		wp_enqueue_script( 'dmi-slick', get_template_directory_uri() . "/static/js/slick.js", array( 'jquery' ), '20160802' );

		wp_enqueue_script( 'dmi-theme', get_template_directory_uri() . "/static/js/site.js", array( 'jquery', 'underscore', 'magnific-popup', 'dmi-slick' ), '20160803' );

	}

	function admin_head_css(){
		?><style type="text/css">
			.mce-ico.fa { font-family: 'FontAwesome', 'Dashicons'; }
		</style><?php
	}

	function tiny_mce_buttons( $buttons ) {
		
		array_unshift( $buttons, 'styleselect' );
		
		return $buttons;

	}

	// Callback function to filter the MCE settings
	function tiny_mce_insert_formats( $init_array ) {  
		// Define the style_formats array
		$style_formats = array(  
			// Each array child is a format with it's own settings
			array(  
				'title'    => 'Button',  
				'selector' => 'a',  
				'classes'  => 'button',
				// font awesome must be available in the admin area to see the icon
				'icon'     => ' fa fa-hand-pointer-o'
			),
		);  
		// Insert the array, JSON ENCODED, into 'style_formats'
		$init_array['style_formats'] = json_encode( $style_formats );  
		
		return $init_array;  
	  
	}

	function pre_get_posts( $query ){
		if ( is_archive() && is_tax( 'business_category' ) && ! is_admin() ){
			$query->set('orderby', 'title' );
			$query->set('order', 'ASC' );
			$query->set('posts_per_page', -1 );
		}
	}

}

new DMISite();

function dmi_render_primary_menu(){ // used in header.twig
	// Twig won't let us call functions with lots of arguments. This is easier!
	// This render method also works correctly with the customizer selective refresh
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container' => '',
	) );
}

/**
* Registers a new post type
* @uses $wp_post_types Inserts new post type object into the list
*
* @param string  Post type key, must not exceed 20 characters
* @param array|string  See optional args description above.
* @return object|WP_Error the registered post type object, or an error object
*/
function dmi_register_business() {

	$labels = array(
		'name'                => __( 'Businesses', 'dmi' ),
		'singular_name'       => __( 'Business', 'dmi' ),
		'add_new'             => _x( 'Add New Business', 'dmi', 'dmi' ),
		'add_new_item'        => __( 'Add New Business', 'dmi' ),
		'edit_item'           => __( 'Edit Business', 'dmi' ),
		'new_item'            => __( 'New Business', 'dmi' ),
		'view_item'           => __( 'View Business', 'dmi' ),
		'search_items'        => __( 'Search Businesses', 'dmi' ),
		'not_found'           => __( 'No Businesses found', 'dmi' ),
		'not_found_in_trash'  => __( 'No Businesses found in Trash', 'dmi' ),
		'parent_item_colon'   => __( 'Parent Business:', 'dmi' ),
		'menu_name'           => __( 'Businesses', 'dmi' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'A collection of all the bussinesses in Downtown Manhattan.',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-store',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'show_in_rest'        => true,
		'capability_type'     => 'post',
		'supports'            => array(
			'title', 'editor'
		)
	);

	register_post_type( 'business', $args );
	
	$labels = array(
		'name'					=> __( 'Categories', 'dmi' ),
		'singular_name'			=> __( 'Category', 'dmi' ),
		'search_items'			=> __( 'Search Categories', 'dmi' ),
		'popular_items'			=> __( 'Popular Categories', 'dmi' ),
		'all_items'				=> __( 'All Categories', 'dmi' ),
		'parent_item'			=> __( 'Parent Category', 'dmi' ),
		'parent_item_colon'		=> __( 'Parent Category', 'dmi' ),
		'edit_item'				=> __( 'Edit Category', 'dmi' ),
		'update_item'			=> __( 'Update Category', 'dmi' ),
		'add_new_item'			=> __( 'Add New Category', 'dmi' ),
		'new_item_name'			=> __( 'New Category Name', 'dmi' ),
		'add_or_remove_items'	=> __( 'Add or remove Categories', 'dmi' ),
		'choose_from_most_used'	=> __( 'Choose from most used Categories', 'dmi' ),
		'menu_name'				=> __( 'Categories', 'dmi' ),
	);

	$args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_admin_column' => true,
		'hierarchical'      => true,
		'show_tagcloud'     => false,
		'show_ui'           => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'categories' ),
		'query_var'         => true,
		'capabilities'      => array(),
	);

	register_taxonomy( 'business_category', array( 'business' ), $args );

	$labels = array(
		'name'                => __( 'Jobs', 'dmi' ),
		'singular_name'       => __( 'Job', 'dmi' ),
		'add_new'             => _x( 'Add New Job', 'dmi', 'dmi' ),
		'add_new_item'        => __( 'Add New Job', 'dmi' ),
		'edit_item'           => __( 'Edit Job', 'dmi' ),
		'new_item'            => __( 'New Job', 'dmi' ),
		'view_item'           => __( 'View Job', 'dmi' ),
		'search_items'        => __( 'Search Jobs', 'dmi' ),
		'not_found'           => __( 'No Jobs found', 'dmi' ),
		'not_found_in_trash'  => __( 'No Jobs found in Trash', 'dmi' ),
		'parent_item_colon'   => __( 'Parent Job:', 'dmi' ),
		'menu_name'           => __( 'Jobs', 'dmi' ),
	);

	$args = array(
		'labels'			  => $labels,
		'hierarchical'        => false,
		'description'         => 'Available Jobs in Downtown Manhattan.',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-hammer',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => array('slug'=> 'jobs'),
		'show_in_rest'        => true,
		'capability_type'     => 'post',
		'supports'            => array(
			'title', 'editor', 'thumbnail'
		)
	);

	register_post_type( 'job', $args );

	$labels = array(
		'name'                => __( 'Properties', 'dmi' ),
		'singular_name'       => __( 'Property', 'dmi' ),
		'add_new'             => _x( 'Add New Property', 'dmi', 'dmi' ),
		'add_new_item'        => __( 'Add New Property', 'dmi' ),
		'edit_item'           => __( 'Edit Property', 'dmi' ),
		'new_item'            => __( 'New Property', 'dmi' ),
		'view_item'           => __( 'View Property', 'dmi' ),
		'search_items'        => __( 'Search Properties', 'dmi' ),
		'not_found'           => __( 'No Properties found', 'dmi' ),
		'not_found_in_trash'  => __( 'No Properties found in Trash', 'dmi' ),
		'parent_item_colon'   => __( 'Parent Property:', 'dmi' ),
		'menu_name'           => __( 'Real Estate', 'dmi' ),
	);

	$args = array(
		'labels'			  => $labels,
		'hierarchical'        => false,
		'description'         => 'Available Real Estate in Downtown Manhattan.',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-admin-multisite',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => array('slug'=> 'real-estate'),
		'show_in_rest'        => true,
		'capability_type'     => 'post',
		'supports'            => array(
			'title', 'editor', 'thumbnail'
		)
	);

	register_post_type( 'realestate', $args );
}

add_action( 'init', 'dmi_register_business' );

/**
 * Display a custom taxonomy dropdown in admin
 * @author Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_action('restrict_manage_posts', 'dmi_filter_post_type_by_taxonomy');
function dmi_filter_post_type_by_taxonomy() {
	global $typenow;
	$post_type = 'business'; // change to your post type
	$taxonomy  = 'business_category'; // change to your taxonomy
	if ($typenow == $post_type) {
		$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
		$info_taxonomy = get_taxonomy($taxonomy);
		wp_dropdown_categories(array(
			'show_option_all' => __("Show All {$info_taxonomy->label}"),
			'taxonomy'        => $taxonomy,
			'name'            => $taxonomy,
			'orderby'         => 'name',
			'hierarchical'    => 1,
			'selected'        => $selected,
			'show_count'      => true,
			'hide_empty'      => true,
		));
	};
}
/**
 * Filter posts by taxonomy in admin
 * @author  Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_filter('parse_query', 'dmi_convert_id_to_term_in_query');
function dmi_convert_id_to_term_in_query($query) {
	global $pagenow;
	$post_type = 'business'; // change to your post type
	$taxonomy  = 'business_category'; // change to your taxonomy
	$q_vars    = &$query->query_vars;
	if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
		$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
		$q_vars[$taxonomy] = $term->slug;
	}
}

function dmi_render_helpful_links_menu(){ // used in footer.twig
	// This render method also works correctly with the customizer selective refresh
	wp_nav_menu( array(
		'theme_location' => 'helpful_links',
		'container' => '',
	) );
}

add_action('admin_footer', function() {
?>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
    	if ( ! $('body').hasClass('post-type-business') )
    		return;
        var $scope = $('#business_categorychecklist');
        
        $('#publish').click(function(){
            if ($scope.find('input:checked').length > 0 || $scope.length === 0) {
                return true;
            } else {
                alert('Please Assign a Business Category');
                return false;
            }
        });
    });
</script>
<?php
});