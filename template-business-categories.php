<?php 
/**
 * Template Name: Top Level Business Categories
 */
$context = Timber::get_context();
$context['post'] = Timber::get_post();
$context['terms'] = Timber::get_terms('business_category');
$context['terms'] = array_filter( $context['terms'], function( $term ){
	return ! $term->parent;
} );

Timber::render( 'business-category-list.twig', $context );