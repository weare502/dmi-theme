<?php
/**
 * Home Template File
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

if ( ! class_exists( 'Timber' ) ) {
	echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
	return;
}
$context = Timber::get_context();

$context['post'] = Timber::get_post();

$context['events'] = get_transient('dmi_frontpage_events');

if ( false === $context['events'] || is_user_logged_in() ) {
	
	$context['events'] = Timber::get_posts( tribe_get_events( array( 
		'posts_per_page' => 10,
		'start_date' => 'now',
		'hide_subsequent_recurrences' => true,
		'eventDisplay' => 'list',
		'featured' => true,
		// 'custom_event_param' => true,
		// 'meta_query' => array(
		// 	array(
		// 		'key'     => 'featured',
		// 		'value'   => true,
		// 		'compare' => '=',
		// 	),
		// ),
	) ), 'DMI_Event' );

	set_transient('dmi_frontpage_events', $context['events'], 120 );
}

$context['businesses'] = get_transient('dmi_frontpage_businesses');

// check to see if data was successfully retrieved from the cache
if ( false === $context['businesses'] || is_user_logged_in()  ) {

	$context['businesses'] = Timber::get_posts( new WP_Query( array(
		'posts_per_page' => 2,
		'post_type' => 'business',
		'orderby' => 'rand',
		// 'meta_key' => 'logo',
		'meta_query' => array(
		    array(
		        'key' => 'logo',
		        'value'   => '',
		        'compare' => '!='
		    )
		),
		'tax_query' => array(
			array(
			    'taxonomy' => 'business_category',
			    'field' => 'term_taxonomy_id',
			    'terms' => array(21),
			    'operator' => 'NOT IN',
			    'include_children' => false
			)
		)
	) ) );

	// store the data and set it to expire in 120 seconds
	set_transient('dmi_frontpage_businesses', $context['businesses'], 120 );

}

$context['property'] = get_transient('dmi_frontpage_property');

// check to see if data was successfully retrieved from the cache
if ( false === $context['property'] || is_user_logged_in()  ) {

	$context['property'] = Timber::get_posts( new WP_Query( array(
		'posts_per_page' => 1,
		'post_type' => 'realestate',
		'orderby' => 'rand',
		'meta_query' => array(
		    array(
		        'key' => '_thumbnail_id',
		        'value'   => '',
		        'compare' => '!='
		    )
		)
	) ) );

	// store the data and set it to expire in 120 seconds
	set_transient('dmi_frontpage_property', $context['property'], 120 );

}

 // var_dump($context['events'][0]->event_dates());

$templates = array( 'front-page.twig' );

Timber::render( $templates, $context );